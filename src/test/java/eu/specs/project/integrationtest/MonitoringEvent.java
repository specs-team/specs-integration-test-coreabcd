package eu.specs.project.integrationtest;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MonitoringEvent {
	
	/*
	 * 
	 * component (string): the unique identifier (usually a UUID) of the component instance which generated the event, 
	 *                     i.e. a virtual machine, a RabbitMQ instance or a web-server instance
	 * object (string): is a hierarchical dot-notation string that pinpoints more accurately the event source within 
	 *                  the 'component', i.e. queue, exchange, etc.
	 * labels (array of strings): a list of hierarchical strings that provides a way to give a context to the event, 
	 *                            i.e. one such label could be vm, to denote all events coming from a VM, or user-x, to 
	 *                            denote all events belonging to a certain user
	 * type (string): an hierarchical string indicating what type of event is this, like "syslog", "structured-log", "cloudwatch.metrics"
	 * data (JSON object): depends on the type of event
	 *     ----> we use (string) for the current implementation
 	 * timestamp (number): time of the event, in seconds
	 * token (JSON object): the token identifying an event generated by the Event Hub. The object has two attributes uuid, 
	 *                      containing the UUID in the token, and seq, containing the sequence number in the token.
	 * 
	 * {
     *	"component":"specs-counter-filter",
     *	"object":"",
     *	"labels":["component1"],
     *	"type":"int",
     * 	"data":"2",
     * 	"timestamp":1414701485
	 * }
	 *
	 */
	
	private String component;
	private String object;
	private ArrayList<String> labels;
	private String type;
	private String data;
	private long timestamp;
	private Object token = null;
	
		
	public MonitoringEvent() {
	}
	
	public String getEventAsJSON(MonitoringEvent event) {
		ObjectMapper mep = new ObjectMapper();

			String meJsonString = null;
			try {
				meJsonString = mep.writerWithDefaultPrettyPrinter().writeValueAsString(event);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return meJsonString;
	}
	
	public boolean parseEventFromArrayOfJSON(String arrayOfEvents) {
		// assuming one element in the array
		// needed by EventArchiver testing
		
		ObjectMapper mem = new ObjectMapper();

			MonitoringEvent[] me = null;
			try {
				me = mem.readValue(arrayOfEvents, MonitoringEvent[].class);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			if (me.length != 1) {
				return false;
			}
			this.component = me[0].component;
			this.object = me[0].object;
			this.labels = me[0].labels;
			this.type = me[0].type;
			this.data = me[0].data;
			this.timestamp = me[0].timestamp;
			this.token = me[0].token;
			return true;
	}
	
	public boolean parseEventFromJSON(String event) {
		ObjectMapper mem = new ObjectMapper();

			MonitoringEvent me = null;
			try {
				me = mem.readValue(event, MonitoringEvent.class);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			//System.out.println(me);
			this.component = me.component;
			this.object = me.object;
			this.labels = me.labels;
			this.type = me.type;
			this.data = me.data;
			this.timestamp = me.timestamp;
			this.token = null;
			return true;
	}
	
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public ArrayList<String> getLabels() {
		return labels;
	}
	public void setLabels(ArrayList<String> labels) {
		this.labels = labels;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public Object getToken() {
		return token;
	}
	public void setToken(Object token) {
		this.token = null;
	}
	
}
