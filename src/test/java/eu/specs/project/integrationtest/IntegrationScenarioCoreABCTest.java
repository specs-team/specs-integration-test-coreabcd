package eu.specs.project.integrationtest;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

public class IntegrationScenarioCoreABCTest {

	private static String bannerTi = "SPECS:coreC1:TestIntegration: ";
	private static String bannerMp = "SPECS:coreC1:TestMonipoli: ";
	private static String bannerEh = "SPECS:coreC1:TestEventHub: ";
	private static String bannerEa = "SPECS:coreC1:TestEventArchiver: ";
	private static String bannerEg = "SPECS:coreC1:EventGenerator: ";
	private static String bannerCtp = "SPECS:coreC2:TestCtp: ";
	
	private static MonitoringConfig config = new MonitoringConfig();
	private static String slaDocument = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	    System.out.println("setUpBeforeClass Called");
	    String ipAddress = System.getenv("IP_ADDRESS");
	    System.out.println("Ip Address: "+ipAddress);
	    
	    if (ipAddress != null) {
	        config.setCtpIP(ipAddress);
	        config.setEventArchiverIP(ipAddress);
	        config.setEventHubIP(ipAddress);
	        config.setMonipoliIP(ipAddress);
	    } else {
	    	throw new AssertionError("Ip Address is null.");
	    }
	    
	    String slaDocumentPath = System.getenv("SLA_DOCUMENT");
	    if (slaDocumentPath == null) {
	    	slaDocumentPath = System.getProperty("user.dir") + "/src/test/resources/sla-test.xml";
	    } 
	    
		try {
			slaDocument = new SLA2String(slaDocumentPath).getSlaDocument();
		} catch (IOException e) {
			System.out.println("--------------------------");
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerTi + "EXCEPTION!");
			System.out.println("--------------------------");
			throw new AssertionError("SLA document (" + slaDocumentPath + ") couldn't be found or open.");
		}
		
		if (slaDocument == null) {
			throw new AssertionError("SLA document (" + slaDocumentPath + ") couldn't be decoded.");
		}
	}
	
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("tearDownAfterClass Called");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown Called");
    }

    @Test
    public void testMonipoli() {
    	MonipoliClient mp = new MonipoliClient(config);
    	try {
			if (mp.registerSlaDocument("new", slaDocument.toString())) {
				if(!mp.testRegisteredSecurityMetrics(slaDocument.toString())) {
					System.out.println("--------------------------------------");
					System.out.println(bannerMp + "All tests: FAILED!");
					System.out.println("--------------------------------------");
					throw new AssertionError(bannerMp + "registered security matrics "
							+ "dosn't match with the ones from the SLA Document!");
				} else {
					System.out.println("--------------------------------------");
					System.out.println(bannerMp + "All tests: Test PASSED!");
					System.out.println("--------------------------------------");
				}
			}
		} catch (XPathExpressionException | IOException | SAXException | ParserConfigurationException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerMp + "EXCEPTION!");
			System.out.println("--------------------------");
			throw new AssertionError(bannerMp + "exception occured! Check the stack trace output!");
		}
    }
    
    @Test
    public void testEventHubArchiver() {
		MonitoringEvent event = injectEventArchiverEvent();
	   	EventArchiverClient ea = new EventArchiverClient(config);
			try {
				if(ea.verifyPublishedEvent(event)) {
					System.out.println("--------------------------");
					System.out.println(bannerEa + "All tests PASSED!");
					System.out.println("--------------------------");
				} else {
					System.out.println("--------------------------");
					System.out.println(bannerEa + "All tests FAILED!");
					System.out.println("--------------------------");
					throw new AssertionError(bannerEa + "the event was not found in the Event Archiver!");
				}
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
				System.out.println("--------------------------");
				System.out.println(bannerEa + "EXCEPTION!");
				System.out.println("--------------------------");
			}
    }
    
    @Test
    public void testCtp() {
    	CtpClient ctp = new CtpClient(config);

    	MonitoringEvent event = injectEventArchiverEvent();
		System.out.println(bannerCtp + "SLA ID: " + event.getLabels().get(0));
		System.out.println(bannerCtp + "Metric Name: " + event.getLabels().get(1));
		try {
			if(ctp.verifyRegisteredSla(event.getLabels().get(0))) {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "SLA registration test PASSED!");
				System.out.println("--------------------------");
			} else {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "SLA registration test FAILED!");
				System.out.println("--------------------------");
				throw new AssertionError(bannerCtp + "SLA ID was not registered on CTP server!");
			}
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerCtp + "EXCEPTION!");
			System.out.println("--------------------------");
			throw new AssertionError(bannerCtp + "exception occured when tried to check "
					+ "if the SLA ID was registered on CTP server.");
		}
		try {
			if(ctp.verifyRegisteredMetric(event.getLabels().get(1))) {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "Metric registration test PASSED!");
				System.out.println("--------------------------");
			} else {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "Metric registration test FAILED!");
				System.out.println("--------------------------");
				throw new AssertionError(bannerCtp + "Metric was not registered on CTP server!");
			}
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerCtp + "EXCEPTION!");
			System.out.println("--------------------------");
			throw new AssertionError(bannerCtp + "exception occured when tried to check "
					+ "if the metric was registered on CTP server.");
		}
    }
    
    private MonitoringEvent injectEventArchiverEvent() {
    	MonitoringEventGenerator ev = new MonitoringEventGenerator(slaDocument);
    	EventHubClient eh = new EventHubClient(config);
    	MonitoringEvent event = null;
    	
		try {
			event = ev.generateRandom(false);
		} catch (XPathExpressionException | SAXException | ParserConfigurationException | IOException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerEg + "EXCEPTION!");
			System.out.println("--------------------------");
			throw new AssertionError(bannerEg + "exception occured when tried to generate an event! "
					+ "Check the stack trace output!");
		}
		
		String _ej = event.getEventAsJSON(event);
		if (_ej != null) {
			System.out.println("--------------------------");
			System.out.println(bannerEh + "Event converted to JSON:");
			System.out.println("--------------------------");
			System.out.println(_ej);
			System.out.println("--------------------------");
		} else {
			System.out.println("--------------------------");
			System.out.println(bannerEh + "Event conversion to JSON failed!");
			System.out.println("--------------------------");
			throw new AssertionError(bannerEh + "event conversion to JSON failed!");
		}
    	
		try {
			if (eh.publishEvent(event)) {
				System.out.println("--------------------------");
				System.out.println(bannerEh + "All tests PASSED!");
				System.out.println("--------------------------");
			} else {
				System.out.println("--------------------------");
				System.out.println(bannerEh + "Event not published!");
				System.out.println("--------------------------");
				throw new AssertionError(bannerEh + "event not published to the EventHub.");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerEh + "EXCEPTION!");
			System.out.println("--------------------------");
			throw new AssertionError(bannerEh + "exception occured when tried to publish the event!");
		}
		return event;
    }
    
}
