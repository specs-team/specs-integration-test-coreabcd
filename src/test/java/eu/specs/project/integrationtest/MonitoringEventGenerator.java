package eu.specs.project.integrationtest;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

public class MonitoringEventGenerator {
	
	/*
	 * 
	 * component (string): the unique identifier (usually a UUID) of the component instance which generated the event, 
	 *                     i.e. a virtual machine, a RabbitMQ instance or a web-server instance
	 * object (string): is a hierarchical dot-notation string that pinpoints more accurately the event source within 
	 *                  the 'component', i.e. queue, exchange, etc.
	 * labels (array of strings): a list of hierarchical strings that provides a way to give a context to the event, 
	 *                            i.e. one such label could be vm, to denote all events coming from a VM, or user-x, to 
	 *                            denote all events belonging to a certain user
	 * type (string): an hierarchical string indicating what type of event is this, like "syslog", "structured-log", "cloudwatch.metrics"
	 * data (JSON object): depends on the type of event
	 *     --------> we use (string) in the current implementation
	 * timestamp (number): time of the event, in seconds
	 * token (JSON object): the token identifying an event generated by the Event Hub. The object has two attributes uuid, 
	 *                      containing the UUID in the token, and seq, containing the sequence number in the token.
	 * 
	 */ 
	
	private String slaDocument = "";
	
	public MonitoringEventGenerator(String slaDocument) {
		this.slaDocument = slaDocument;
	}
		
	public MonitoringEvent generateRandom(boolean alert) throws XPathExpressionException, SAXException, ParserConfigurationException, IOException {

		int no = 1;
		MonitoringEvent _event = null;
		SLA2SecurityMetrics ssm = new SLA2SecurityMetrics();
		ArrayList<MetricSLOMap> sm = ssm.getSecurityMetrics(this.slaDocument);
		long seed = System.nanoTime();
		Collections.shuffle(sm, new Random(seed));
		int count = 0;
		while (count < no) {
			MetricSLOMap msm = sm.get(this.getRandomNumber(0, sm.size()-1));
			String _object = msm.getName();
			String _component = this.getRandomObject();
			ArrayList<String> _labels = new ArrayList<String>();
			_labels.add("sla_id_" + msm.getSlaId());
			_labels.add("security_metric_" + msm.getMetricReference());
			String _type = this.getOperandType(msm.getOperand());
			/*
			HashMap<String, String> _ddata = new HashMap<String, String>();
			if (alert) {
				if (this.getOperandType(msm.getOperand()).equals("boolean")) {
					if (msm.getOperand().equals("true")) {
						_ddata.put(msm.getOperator(), "false");
					} else {
						_ddata.put(msm.getOperator(), "true");
					}
				} else if (this.getOperandType(msm.getOperand()).equals("int")) {
					if (msm.getOperator().equals("eq")) {
						_ddata.put(msm.getOperator(), String.valueOf(Integer.parseInt(msm.getOperand())+3));
					}
					if (msm.getOperator().equals("gt") || msm.getOperator().equals("geq")) {
						_ddata.put(msm.getOperator(), String.valueOf(Integer.parseInt(msm.getOperand())-3));
					}
					if (msm.getOperator().equals("lt") || msm.getOperator().equals("leq")) {
						_ddata.put(msm.getOperator(), String.valueOf(Integer.parseInt(msm.getOperand())+3));
					}
				} else {
					_ddata.put(msm.getOperator(), "");
				}
			} else {
				if (this.getOperandType(msm.getOperand()).equals("boolean")) {
					_ddata.put(msm.getOperator(), msm.getOperand());
				} else if (this.getOperandType(msm.getOperand()).equals("int")) {
					if (msm.getOperator().equals("eq")) {
						_ddata.put(msm.getOperator(), msm.getOperand());
					}
					if (msm.getOperator().equals("gt") || msm.getOperator().equals("geq")) {
						_ddata.put(msm.getOperator(), String.valueOf(Integer.parseInt(msm.getOperand())+1));
					}
					if (msm.getOperator().equals("lt") || msm.getOperator().equals("leq")) {
						_ddata.put(msm.getOperator(), String.valueOf(Integer.parseInt(msm.getOperand())-1));
					}
				} else {
					_ddata.put(msm.getOperator(), msm.getOperand());
				}
			}
			ArrayList<HashMap<String,String>> _data = new ArrayList<HashMap<String,String>>();
			_data.add(_ddata);
			*/
			String _data = null;
			if (alert) {
				if (this.getOperandType(msm.getOperand()).equals("boolean")) {
					if (msm.getOperand().equals("true")) {
						_data = "false";
					} else {
						_data = "true";
					}
				} else if (this.getOperandType(msm.getOperand()).equals("int")) {
					if (msm.getOperator().equals("eq")) {
						_data = String.valueOf(Integer.parseInt(msm.getOperand())+3);
					}
					if (msm.getOperator().equals("gt") || msm.getOperator().equals("geq")) {
						_data = String.valueOf(Integer.parseInt(msm.getOperand())-3);
					}
					if (msm.getOperator().equals("lt") || msm.getOperator().equals("leq")) {
						_data = String.valueOf(Integer.parseInt(msm.getOperand())+3);
					}
				} else {
					_data = "";
				}
			} else {
				if (this.getOperandType(msm.getOperand()).equals("boolean")) {
					_data = msm.getOperand();
				} else if (this.getOperandType(msm.getOperand()).equals("int")) {
					if (msm.getOperator().equals("eq")) {
						_data = msm.getOperand();
					}
					if (msm.getOperator().equals("gt") || msm.getOperator().equals("geq")) {
						_data = String.valueOf(Integer.parseInt(msm.getOperand())+1);
					}
					if (msm.getOperator().equals("lt") || msm.getOperator().equals("leq")) {
						_data = String.valueOf(Integer.parseInt(msm.getOperand())-1);
					}
				} else {
					_data =  msm.getOperand();
				}
			}
			long _timestamp = this.getTimestamp();
			_event = this.generateEvent(_component, _object, _labels, _type, _data, _timestamp);
			count +=1;
		}
		return _event;
	}
	
	private MonitoringEvent generateEvent(String component, String object, ArrayList<String> labels, String type, String data, long timestamp) {
		
		MonitoringEvent event = new MonitoringEvent();
		
		event.setComponent(component);
		event.setObject(object);
		event.setLabels(labels);
		event.setType(type);
		event.setTimestamp(timestamp);
		event.setData(data);
		return event;
	}
	
	private long getTimestamp() {
		return Instant.now().getEpochSecond();
	}

	private String getRandomObject() {
		return UUID.randomUUID().toString();
	}
	
	private String getOperandType(String operand) {
		if ((operand.equals("true")) || operand.equals(operand.equals("false"))){
			return "boolean";
		} else {
			try {
				Integer.parseInt(operand);
				return "int";
			} catch (Exception ex) {
				return "";
			}
		}
	}
	
	private int getRandomNumber(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}