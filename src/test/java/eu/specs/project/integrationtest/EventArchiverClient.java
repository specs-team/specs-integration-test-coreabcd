package eu.specs.project.integrationtest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

public class EventArchiverClient {
	
	private String banner = "SPECS:coreC1:EventArchiverClient: ";
	
	private MonitoringConfig configuration = new MonitoringConfig();
	
	public EventArchiverClient(MonitoringConfig configuration) {
		this.configuration = configuration;
	}
	
	public boolean verifyPublishedEvent(MonitoringEvent event) throws ClientProtocolException, IOException, URISyntaxException {
		
		String eaURL = "http://" + configuration.getEventArchiverIP() + ":" + configuration.getEventArchiverPort()
		+ configuration.getEventArchiverBaseURL();
		String eaSearchQuery = "filter={\"component\":\"" + event.getComponent() + "\"}";
		String eaURI = new URIBuilder().setCustomQuery(eaSearchQuery).build().toString();
		//System.out.println(eaURI);
		HttpClient eaClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(eaURL+eaURI);
		request.addHeader("User-Agent", "SPECS Integration - Monitoring Core C");
		HttpResponse response = eaClient.execute(request);
		System.out.println(banner + "HTTP_GET CODE: " 
                + response.getStatusLine().getStatusCode());
		
		if (response.getStatusLine().getStatusCode() != 200) {
			return false;
		}
		
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
		
		String line = "";
		StringBuffer result = new StringBuffer();
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		//System.out.println(result.toString().trim());
		
		MonitoringEvent me = new MonitoringEvent();
		if (!me.parseEventFromArrayOfJSON(result.toString().trim())) {
			return false;
		}
		
		if (!me.getComponent().equals(event.getComponent()) || 
			!me.getData().equals(event.getData()) ||
			!me.getObject().equals(event.getObject()) ||
			me.getTimestamp() != event.getTimestamp())
			return false;
		
		return true;
	}
	
}