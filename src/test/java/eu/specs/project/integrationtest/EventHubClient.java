package eu.specs.project.integrationtest;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.*;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class EventHubClient {
	
	private MonitoringConfig configuration = new MonitoringConfig();
	
	public EventHubClient(MonitoringConfig configuration) {
		this.configuration = configuration;
	}

	public boolean publishEvent(MonitoringEvent event) throws ClientProtocolException, IOException {
		
		String banner = "SPECS:coreC1:EventHubClient: ";
		
		String ehURL = "http://" + configuration.getEventHubIP() + ":" + configuration.getEventHubPort() 
						+ configuration.getEventHubBaseURL();
		
		HttpClient ehClient = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(ehURL);
		request.addHeader("User-Agent", "SPECS Integration - Monitoring Core C");
		request.addHeader("Content-Type", "application/json");
		HttpEntity et = new ByteArrayEntity(event.getEventAsJSON(event).getBytes());
		request.setEntity(et);
		HttpResponse response = ehClient.execute(request);
		
		System.out.println(banner + "ehPost:Response Code: " 
                + response.getStatusLine().getStatusCode());
		
		if (response.getStatusLine().getStatusCode() != 200) {
			return false;
		}
		return true;
	}
}