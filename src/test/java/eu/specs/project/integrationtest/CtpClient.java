package eu.specs.project.integrationtest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CtpClient {

	private String banner = "SPECS:coreC2:CtpClient: ";
	
	private MonitoringConfig configuration = new MonitoringConfig();
	
	public CtpClient(MonitoringConfig configuration) {
		this.configuration = configuration;
	}
	
	public boolean verifyRegisteredSla(String slaId) throws URISyntaxException, ClientProtocolException, IOException {
		String ctpURL = "http://" + configuration.getCtpIP() + ":" + configuration.getCtpPort() + configuration.getCtpBaseURL() + configuration.getCtpSlaViewURL();
		String ctpSearchQuery = "name=" + slaId;
		String ctpURI = new URIBuilder().setCustomQuery(ctpSearchQuery).build().toString();
		//System.out.println(ctpURI);
		HttpClient ctpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(ctpURL+ctpURI);
		request.addHeader("User-Agent", "SPECS Integration - Monitoring Core C");
		request.addHeader("Authorization", "Bearer 0000");
		HttpResponse response = ctpClient.execute(request);
		System.out.println(banner + "HTTP_GET CODE: " 
                + response.getStatusLine().getStatusCode());
		
		if (response.getStatusLine().getStatusCode() != 200) {
			return false;
		}
		
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
		
		String line = "";
		StringBuffer result = new StringBuffer();
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		ObjectMapper mem = new ObjectMapper();
		try {
			JsonNode me = mem.readValue(String.valueOf(result).getBytes(), JsonNode.class);
			if ((me.get("collectionLength").asInt() != 1) && (me.get("collectionType").asText() == "serviceViews")) {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean verifyRegisteredMetric(String metricName) throws URISyntaxException, ClientProtocolException, IOException {
		String ctpURL = "http://" + configuration.getCtpIP() + ":" + configuration.getCtpPort() + configuration.getCtpBaseURL() + configuration.getCtpMetricViewURL();
		String ctpSearchQuery = "name=" + metricName;
		String ctpURI = new URIBuilder().setCustomQuery(ctpSearchQuery).build().toString();
		//System.out.println(ctpURI);
		HttpClient ctpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(ctpURL+ctpURI);
		request.addHeader("User-Agent", "SPECS Integration - Monitoring Core C");
		request.addHeader("Authorization", "Bearer 0000");
		HttpResponse response = ctpClient.execute(request);
		System.out.println(banner + "HTTP_GET CODE: " 
                + response.getStatusLine().getStatusCode());
		
		if (response.getStatusLine().getStatusCode() != 200) {
			return false;
		}
		
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
		
		String line = "";
		StringBuffer result = new StringBuffer();
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		ObjectMapper mem = new ObjectMapper();
		try {
			JsonNode me = mem.readValue(String.valueOf(result).getBytes(), JsonNode.class);
			if ((me.get("collectionLength").asInt() != 1) && (me.get("collectionType").asText() == "metrics")) {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}