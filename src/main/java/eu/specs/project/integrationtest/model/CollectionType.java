package eu.specs.project.integrationtest.model;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collectionType", propOrder = {
        "itemList"
})
public class CollectionType {

    protected List<CollectionType.ItemList> itemList;
    @XmlAttribute(name = "resource", required = true)
    protected String resource;
    @XmlAttribute(name = "total")
    protected BigInteger total;
    @XmlAttribute(name = "members")
    protected BigInteger members;

    public List<CollectionType.ItemList> getItemList() {
        if (itemList == null) {
            itemList = new ArrayList<ItemList>();
        }
        return this.itemList;
    }

    public String getResource() {
        return resource;
    }

    public BigInteger getTotal() {
        return total;
    }

    public BigInteger getMembers() {
        return members;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    public static class ItemList {

        @XmlValue
        @XmlSchemaType(name = "anyURI")
        protected String value;
        @XmlAttribute(name = "id")
        protected String id;

        public String getValue() {
            return value;
        }

        public String getId() {
            return id;
        }
    }
}
