package eu.specs.project.integrationtest.main;

import java.io.IOException;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SpecsIntegrationTest {
	
	private static 	WebDriver driver;
	private static  Logger logger;
	
	public static void main (String[] args) throws Exception {
		SpecsIntegrationTest 	SIT = new SpecsIntegrationTest();
		
		System.setProperty("webdriver.chrome.driver", "/home/specs/bin/chromedriver");
		
		driver.get("http://localhost:8080/sws.demo/");
		logger.log(Level.INFO, "Page title is {0}", driver.getTitle());
		
		SIT.testSequence1(SIT);

		SIT.testSequence2(SIT);

		SIT.testSequence3(SIT);

		SIT.testSequence4(SIT);
		
		Thread.sleep(3000);
		driver.quit();
	}
	
	/*
	 * @brief	Check that all capabilities are available for selection
	 */
	public void
	restart(
			SpecsIntegrationTest SIT
	) {		
		WebElement element = driver.findElement(By.id("specs-home"));
		element.click();
		
		element = driver.findElement(By.id("specs-start-negotiation"));
		element.click();
		logger.log(Level.INFO, "Page title is {0}", driver.getTitle());
	
		SIT.clickNextTop();
		
		// Service Selection Tab: Only option is ViPR
		element = driver.findElement(By.id("SDT_0"));
		if (element.isDisplayed()) {
			logger.log(Level.INFO, "Check box visible");
		}
		SIT.clickNextTop();
	}
	
	/*
	 * @brief	Check that all capabilities are available for selection
	 */
	public void
	testSequence1(
		SpecsIntegrationTest SIT
	) {
		int 	numTests = 0;
		int 	failed = 0;

		logger.log(Level.INFO, "Beginning Test Sequence 1");
		SIT.restart(SIT);
		
		numTests++;
		if (false == SIT.doesElementExist("capability_OSSEC")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("capability_ViPRPERF")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("capability_ViPRSEC")) {
			failed++;
		}

		logger.log(Level.INFO, "Total: {0}", numTests);
		logger.log(Level.INFO, "Passed: {0}", (numTests-failed));
		logger.log(Level.INFO, "Failed: {0}", failed);
	}

	/*
	 * @brief	Test the Security Storage Capabilities path
	 */
	public void
	testSequence2(
		SpecsIntegrationTest SIT
	) {
		int 	numTests = 0;
		int 	failed = 0;
		String	importance = "MEDUIM";
		String 	region = "IE-C";

		logger.log(Level.INFO, "Beginning Test Sequence 2");
		SIT.restart(SIT);
		
		WebElement element = driver.findElement(By.id("capability_OSSEC"));
		element.click();
		clickNextTop();
		
		numTests++;
		if (false == SIT.findAndClickElement("SC_CB_WEBPOOL_CCM_BCR_06")) {
			failed++;
		}
		clickNextTop();		
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M2001")) {
			failed++;
		}
		
		numTests++;
		if (false == SIT.doesElementExist("SLO_IMPORTANCE_specs_ngDC_M2001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_IMPORTANCE_specs_ngDC_M2001"));
			numTests++;
			importance = "4LO3W";
			if (false == SIT.selectFromDropdownFalse(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "LOW";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "HIGH";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "MEDIUM";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
		}
		
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_OP_specs_ngDC_M2001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_OP_specs_ngDC_M2001"));
			numTests++;
			if (false == SIT.selectFromDropdown(element, "less than")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "greater than")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "less than or equal")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "greater than or equal")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "equal")) {
				failed++;
			}
		}
 
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M2001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M2001"));
			element.clear();
			element.sendKeys(region);
		}
		
		// Move to SLA Overview Tab
		clickNextTop();		
		// TODO: Check the SLA list against expected values
		// TODO: Test the Download button?
		numTests++;
		if (false == SIT.findAndClickElement("specs-submit-sla-top")) {
			failed++;
		}
		
		// Find the latest SLA (the one we just created)
		int SLAId = SIT.getLatestSLAId();
		
		// Sign the SLA
		numTests++;
		if (false == SIT.tableClickSLA(SLAId)) {
			failed++;
		}		
		
		// Implement the SLA
		numTests++;
		if (false == SIT.tableClickSLA(SLAId)) {
			failed++;
		}		
		
		// Verify SLA exists under monitoring
		numTests++;
		if (false == SIT.clickMonitorSLA(SLAId)) {
			failed++;
		}		
		
		// Check is the SLO correct?

		logger.log(Level.INFO, "Total: {0}",numTests);
		logger.log(Level.INFO, "Passed: {0}", (numTests-failed));
		logger.log(Level.INFO, "Failed: {0}", failed);
		System.out.println("Total: " + numTests + " | Passed: " + (numTests-failed) + " | Failed: " + failed);
	}

	/*
	 * @brief	Test the Performance Capabilities path
	 */
	public void
	testSequence3(
		SpecsIntegrationTest SIT
	) {
		int 	numTests = 0;
		int 	failed = 0;
		String	importance = "MEDUIM";

		logger.log(Level.INFO, "Beginning Test Sequence 3");
		SIT.restart(SIT);
		
		WebElement element = driver.findElement(By.id("capability_ViPRPERF"));
		element.click();
		clickNextTop();
		
		numTests++;
		if (false == SIT.findAndClickElement("SC_CB_WEBPOOL_CCM_BCR_09")) {
			failed++;
		}
		numTests++;
		if (false == SIT.findAndClickElement("SC_CB_WEBPOOL_CCM_BCR_11")) {
			failed++;
		}
		numTests++;
		if (false == SIT.findAndClickElement("SC_CB_WEBPOOL_CCM_IVS_04")) {
			failed++;
		}
		numTests++;
		if (false == SIT.findAndClickElement("SC_CB_WEBPOOL_CCM_IVS_09")) {
			failed++;
		}
		clickNextBtm();		
		
		/*
		 * Select SLOs
		 */
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M1001")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_IMPORTANCE_specs_ngDC_M1001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_IMPORTANCE_specs_ngDC_M1001"));
			numTests++;
			importance = "4LO3W";
			if (false == SIT.selectFromDropdownFalse(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "LOW";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "HIGH";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "MEDIUM";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_OP_specs_ngDC_M1001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_OP_specs_ngDC_M1001"));
			numTests++;
			if (false == SIT.selectFromDropdown(element, "less than")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "greater than")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "less than or equal")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "greater than or equal")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "equal")) {
				failed++;
			}
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M1001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M1001"));
			element.clear();
			element.sendKeys("Thick");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M1002")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M1002")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M1002"));
			element.clear();
			element.sendKeys("iSCSI");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M1003")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M1003")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M1003"));
			element.clear();
			element.sendKeys("FC");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M1004")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M1004")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M1004"));
			element.clear();
			element.sendKeys("vmax");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M1005")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M1005")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M1005"));
			element.clear();
			element.sendKeys("1");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M1006")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M1006")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M1006"));
			element.clear();
			element.sendKeys("2");
		}
		
		clickNextBtm();
		
		/*
		 *  SLA Overview Tab
		 */
		numTests++;
		if (false == SIT.findAndClickElement("specs-submit-sla-top")) {
			failed++;
		}
		
		// Find the latest SLA (the one we just created)
		int SLAId = SIT.getLatestSLAId();
		
		// Sign the SLA
		numTests++;
		if (false == SIT.tableClickSLA(SLAId)) {
			failed++;
		}		
		
		// Implement the SLA
		numTests++;
		if (false == SIT.tableClickSLA(SLAId)) {
			failed++;
		}		
		
		// Verify SLA exists under monitoring
		numTests++;
		if (false == SIT.clickMonitorSLA(SLAId)) {
			failed++;
		}		
		
		// Check is the SLO correct?

		logger.log(Level.INFO, "Total: {0}",numTests);
		logger.log(Level.INFO, "Passed: {0}", (numTests-failed));
		logger.log(Level.INFO, "Failed: {0}", failed);
		System.out.println("Total: " + numTests + " | Passed: " + (numTests-failed) + " | Failed: " + failed);
	}
	


	/*
	 * @brief	Test the Security Capabilities path
	 */
	public void
	testSequence4(
		SpecsIntegrationTest SIT
	) {
		int 	numTests = 0;
		int 	failed = 0;
		String	importance = "MEDUIM";

		logger.log(Level.INFO, "Beginning Test Sequence 4");
		SIT.restart(SIT);
		
		WebElement element = driver.findElement(By.id("capability_ViPRSEC"));
		element.click();
		clickNextTop();
		
		numTests++;
		if (false == SIT.findAndClickElement("SC_CB_WEBPOOL_CCM_BCR_01")) {
			failed++;
		}
		numTests++;
		if (false == SIT.findAndClickElement("SC_CB_WEBPOOL_CCM_BCR_09")) {
			failed++;
		}
		clickNextBtm();		
		
		/*
		 * Select SLOs
		 */
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M0001")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_IMPORTANCE_specs_ngDC_M0001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_IMPORTANCE_specs_ngDC_M0001"));
			numTests++;
			importance = "4LO3W";
			if (false == SIT.selectFromDropdownFalse(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "LOW";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "HIGH";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
			numTests++;
			importance = "MEDIUM";
			if (false == SIT.selectFromDropdown(element, importance)) {
				failed++;
			}
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_OP_specs_ngDC_M0001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_OP_specs_ngDC_M0001"));
			numTests++;
			if (false == SIT.selectFromDropdown(element, "less than")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "greater than")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "less than or equal")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "greater than or equal")) {
				failed++;
			}
			numTests++;
			if (false == SIT.selectFromDropdown(element, "equal")) {
				failed++;
			}
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M0001")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M0001"));
			element.clear();
			element.sendKeys("RAID1");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M0002")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M0002")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M0002"));
			element.clear();
			element.sendKeys("TRUE");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M0003")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M0003")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M0003"));
			element.clear();
			element.sendKeys("Array");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M0004")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M0004")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M0004"));
			element.clear();
			element.sendKeys("1");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M0005")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M0005")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M0005"));
			element.clear();
			element.sendKeys("1");
		}
		
		numTests++;
		if (false == SIT.findAndClickElement("METRICS_CB_specs_ngDC_M0006")) {
			failed++;
		}
		numTests++;
		if (false == SIT.doesElementExist("SLO_EXP_specs_ngDC_M0006")) {
			failed++;
		}
		else {
			element = driver.findElement(By.id("SLO_EXP_specs_ngDC_M0006"));
			element.clear();
			element.sendKeys("1");
		}
		
		clickNextBtm();
		
		/*
		 *  SLA Overview Tab
		 */
		numTests++;
		if (false == SIT.findAndClickElement("specs-submit-sla-top")) {
			failed++;
		}
		
		// Find the latest SLA (the one we just created)
		int SLAId = SIT.getLatestSLAId();
		
		// Sign the SLA
		numTests++;
		if (false == SIT.tableClickSLA(SLAId)) {
			failed++;
		}		
		
		// Implement the SLA
		numTests++;
		if (false == SIT.tableClickSLA(SLAId)) {
			failed++;
		}		
		
		// Verify SLA exists under monitoring
		numTests++;
		if (false == SIT.clickMonitorSLA(SLAId)) {
			failed++;
		}		
		
		// Check is the SLO correct?

		logger.log(Level.INFO, "Total: {0}",numTests);
		logger.log(Level.INFO, "Passed: {0}", (numTests-failed));
		logger.log(Level.INFO, "Failed: {0}", failed);
		System.out.println("Total: " + numTests + " | Passed: " + (numTests-failed) + " | Failed: " + failed);
	}

	/*
	 * @brief	Constructor
	 * 
	 * @return	void
	 */
	public SpecsIntegrationTest() {
		driver = new ChromeDriver();
		
		try {
			FileHandler handler = new FileHandler("test.log", 50000, 1);
			handler.setFormatter(new SimpleFormatter());
			logger = Logger.getLogger("SPECSLogger");
			logger.addHandler(handler);
		}
		catch (IOException e){
			e.printStackTrace();
		}
		
	}
	
	public void 
	clickNextTop() {
		try {
			WebElement element = driver.findElement(By.id("specs-negotiation-next-top"));
			element.click();
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Element specs-negotiation-next-top missing");
			//res = false;
		}
		catch (org.openqa.selenium.WebDriverException e) {	
			logger.log(Level.SEVERE, "Element specs-negotiation-next-top missing");
			//res = false;
		}
	}
	
	public void 
	clickNextBtm() {
		try {
			WebElement element = driver.findElement(By.id("specs-negotiation-next-btm"));
			element.click();
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Element specs-negotiation-next-btm missing");
			//res = false;
		}
		catch (org.openqa.selenium.WebDriverException e) {	
			logger.log(Level.SEVERE, "Element specs-negotiation-next-btm missing");
			//res = false;
		}
	}
	
	public boolean
	doesElementExist(
		String elementId
	) {
		boolean res = true;
		
		try {
			WebElement element = driver.findElement(By.id(elementId));
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Element ({0}) missing", elementId);
			res = false;
		}
		
		return res;
	}
	
	public boolean
	findAndClickElement(
		String elementId
	) {		
		boolean res = true;
		
		try {
			WebElement element = driver.findElement(By.id(elementId));
			element.click();
		}
		catch (org.openqa.selenium.StaleElementReferenceException e) {	
			logger.log(Level.SEVERE, "Element ({0}) cannot be clicked", elementId);
			res = false;
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Element ({0}) cannot be clicked", elementId);
			res = false;
		}
		
		return res;
	}
	
	public boolean
	selectFromDropdown(
		WebElement element,
		String value
	) {		
		boolean res = true;
		
		try {
			Select sel = new Select(element);
			sel.selectByVisibleText(value);
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Option ({0}) not available in dropdown list", value);
			res = false;
		}
		
		return res;
	}
	
	public boolean
	selectFromDropdownFalse(
		WebElement element,
		String value
	) {		
		boolean res = false;
		
		try {
			Select sel = new Select(element);
			sel.selectByVisibleText(value);
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.INFO, "Option ({0}) not available in dropdown list", value);
			res = true;
		}
		
		return res;
	}
	
	/*
	 * @brief	Loop through the table for the latest SLA ID
	 * 
	 * @return	The ID of the latest SLA
	 */
	public int
	getLatestSLAId() {
		int 	latestSLAId = 0;
		int 	cellValue = 0;
		String 	elementText = "specs-sla-table";
		
		try {
			WebElement table = driver.findElement(By.id(elementText));
			List<WebElement> rows = table.findElements(By.xpath("id('specs-sla-table')/tbody/tr"));
		
			// Find the highest SLA value (corresponding the to inputs we just provided)
			for (WebElement rowElement : rows) {
				List<WebElement> cols = rowElement.findElements(By.xpath("td"));
				cellValue = Integer.parseInt(cols.get(0).getText());
				if (cellValue > latestSLAId) {
					latestSLAId = cellValue;
				}
			}
			//System.out.println("Highest SLA ID is " + latestSLAId);
		}
		catch (org.openqa.selenium.StaleElementReferenceException e) {	
			logger.log(Level.SEVERE, "Table Button cannot be clicked");
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Element ({0}) cannot be found", elementText);
		}
		
		return cellValue;
	}
	
	/*
	 * @brief	Loop through the table for the latest SLA ID and then click 
	 * 			the "Sign/Implement SLA" button contained inside it
	 * 
	 * @param	[in] SLAId		ID of the SLA to use
	 * 
	 * @note	Same code works for both the Sign and Implement pages
	 */
	public boolean
	tableClickSLA(
		int	SLAId
	) {
		boolean res = true;
		int 	row_num = 1;
		int 	SLARow = 0;
		int 	cellValue = 0;
		String 	elementText = "specs-sla-table";
		
		try {
			WebElement table = driver.findElement(By.id(elementText));
			List<WebElement> rows = table.findElements(By.xpath("id('specs-sla-table')/tbody/tr"));
			
			// Find row corresponding to the selected SLA ID
			for (WebElement rowElement : rows) {
				List<WebElement> cols = rowElement.findElements(By.xpath("td"));
				cellValue = Integer.parseInt(cols.get(0).getText());
				if (cellValue == SLAId) {
					SLARow = row_num;
				}
				row_num++;
			}
			//System.out.println("SLA " + SLAId + " found on row " + SLARow);
			elementText = "id('specs-sla-table')/tbody/tr[" + SLARow + "]/td[2]/button";
			WebElement cellButton = table.findElement(By.xpath(elementText));
			cellButton.click();
		}
		catch (org.openqa.selenium.StaleElementReferenceException e) {	
			logger.log(Level.SEVERE, "Table Button cannot be clicked");
			res = false;
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Element ({0}) cannot be found", elementText);
			res = false;
		}
		
		return res;
	}
	

	/*
	 * @brief	Loop through the table for the latest SLA ID and then click 
	 * 			the "Monitor" button contained inside it
	 * 
	 * @param	[in] SLAId		ID of the SLA to use
	 */
	public boolean
	clickMonitorSLA(
		int	SLAId
	) {
		boolean res = true;
		String 	elementText = "signed_sla";
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 4);
			
			// The list can take a few seconds to update
			Thread.sleep(4000);
			WebElement element = driver.findElement(By.id("force_update"));
			element.click();
			
			elementText = SLAId + "_button";
			element = wait.until(ExpectedConditions.elementToBeClickable(By.id(elementText)));
			element.click();
		}
		catch (org.openqa.selenium.StaleElementReferenceException e) {	
			logger.log(Level.SEVERE, "Table Button cannot be clicked");
			res = false;
		}
		catch (org.openqa.selenium.NoSuchElementException e) {	
			logger.log(Level.SEVERE, "Element ({0}) cannot be found", elementText);
			res = false;
		}
		catch (InterruptedException e) {	
			logger.log(Level.SEVERE, "Thread sleep error");
			res = false;
		}
		
		return res;
	}

}

