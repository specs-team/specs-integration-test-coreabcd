# Integration scenarios CoreABCD

This family of scenarios integrates components that are involved in the SLA negotiation, SLA enforcement, SLA monitoring, and SLA remediation phases. More information about integration scenarios can be found in deliverables D1.5.1 and D1.5.2.

### Core-ABCD1
This scenario merges the Core-ABC1 and Core-CD1 integration scenarios, and enables the basic version of the entire SPECS flow (all steps except SLA ranking).

Details:
- Base Scenario ID: Core-ABC1, Core-CD1
- Added artefacts: /

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD2
This scenario extends the Core-ABCD1 scenario by integrating the Security Reasoner component (Negotiation module). By including functionalities related to the evaluation and ranking of the SLAs, this scenario enables the complete SPECS flow.

Details:
- Base Scenario ID: Core-ABCD1
- Added artefacts: Security Reasoner

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD3
This scenario extends the Core-ABCD2 scenario by integrating the Security Tokens mechanism (component of the Vertical Layer), which is responsible for the security of interactions among SPECS components.

Details:
- Base Scenario ID: Core-ABCD2
- Added artefacts: Security Tokens

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: Security Tokens
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD4
This scenario extends the Core-ABCD3 scenario by integrating the User Manager component (Vertical Layer), which oversees authentication and authorization functionalities to SPECS users.

Details:
- Base Scenario ID: Core-ABCD3
- Added artefacts: User Manager

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: Security Tokens, User Manager
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD5
This scenario extends the Core-ABCD4 scenario by integrating the Interoperability Layer component (Vertical Layer), which offers the single access point to all SPECS APIs.

Details:
- Base Scenario ID: Core-ABCD4
- Added artefacts: Interoperability Layer

Involved components:
- SLA Platform:	SLA Manager, Service Manager, Interoperability Layer
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: Security Tokens, User Manager
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD6
This scenario extends the Core-ABCD5 scenario by integrating the Credential Service mechanism (Vertical Layer), which stores and manages SPECS Owner credentials to access the external resources.

Details:
- Base Scenario ID: Core-ABCD5
- Added artefacts: Credential Service

Involved components:
- SLA Platform:	SLA Manager, Service Manager, Interoperability Layer
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: Security Tokens, User Manager, Credential Service
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD7
This scenario extends the Core-ABCD6 scenario by integrating the Auditing component (Vertical Layer), which offers logging functionalities to all components of the SPECS framework.

Details:
- Base Scenario ID: Core-ABCD6
- Added artefacts: Auditing

Involved components:
- SLA Platform:	SLA Manager, Service Manager, Interoperability Layer
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: Security Tokens, User Manager, Credential Service, Auditing
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD8
This scenario extends the Core-ABCD7 scenario by integrating the Nmap mechanism (Monitoring module), which monitors availability of internal components of the SPECS framework.

Details:
- Base Scenario ID: Core-ABCD7
- Added artefacts: Nmap

Involved components:
- SLA Platform:	SLA Manager, Service Manager, Interoperability Layer
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP, Nmap
- Vertical Layer: Security Tokens, User Manager, Credential Service, Auditing
- Security mechanisms: /
- SPECS applications: /

### Core-ABCD9
This scenario extends the Core-ABCD7 scenario by integrating the Nmap mechanism (Monitoring module), which monitors availability of internal components of the SPECS framework.

Details:
- Base Scenario ID: Core-ABCD8
- Added artefacts: Default SPECS Application

Involved components:
- SLA Platform:	SLA Manager, Service Manager, Interoperability Layer
- Negotiation module: SLO Manager, Supply Chain Manager, Security Reasoner
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP, Nmap
- Vertical Layer: Security Tokens, User Manager, Credential Service, Auditing
- Security mechanisms: /
- SPECS applications: Default SPECS Application